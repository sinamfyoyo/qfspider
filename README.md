# 千帆爬虫

[管理端Demo](https://s.4amteam.com/portal/) · [PUSH目标Demo](https://t.4amteam.com/portal/)

#### · 软件架构
使用Python3开发，基于Scrapy-Redis分布式爬虫框架开发，基于Python良好的依赖环境，内置了TF-IDF算法，该算法实现了被抓去文章的热词权重比，并以图形方式展示，详情见TF-IDF章节。

在时效性方面，通过对Scrapy爬虫的信道进行了监控，一旦发现待爬队列关键为空，即将重新开始队列待爬对象。

爬虫效率方面，演示站点为5QPS效率，在服务器配置支持的情况下，配上代理，可支持更高QPS或并发。在单机 **8C16G** 上对目标站实现了500QPS（30,000 items/min）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0131/235148_300a0876_1201544.png "屏幕截图.png")
------------


#### · 管理端
采用了PHP开发，摒弃View视图层，独立Api层。layer admin作为UI端；内置了CURD方式查询，方便高效二次开发。

于Python端通过Redis进行数据交互，使用hset与lpush。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0131/235217_7c6ace3c_1201544.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0131/235308_5321eb52_1201544.png "屏幕截图.png")
------------

#### · 图片转存、视频转存
1、微信内容的图片为data-src的方式，为了效率，放弃了使用无头浏览器加载方式。在Xpath支持下，通过节点将整个图片替换了最简单的<img>标签

2、在图片上的转存上，默认仅支持Tencent-COS。可自行配置CDN的跨域、COS回源等

~~3、视频转存仅支持微信素材视频，不支持腾讯视频的转存~~


------------


#### · 部分特点
>  1、基于hset、hash去重

>  2、信号循环管理，非轮询

>  3、通过source\plugin - API 插件发帖，不需要对原站点修改、不安任何插件

>  4、不同公众号可自动关联账号或自动创建、指定账号自动发布

>  5、分布式队列

>  6、TF-IDF内容分析算法

>  7、持续更新


------------


#### · 反爬说明
1、该爬虫基于sogou微信搜索，在相同SNUID下高并发会触发验证码；在爬虫中间件中自带了打码效验

2、隧道代理分发单IP的请求数

3、随机UA请求

------------


#### · 数据
1、在下载器中使用了Twisted异步pdmysql
2、数据发布中使用了基于Thread的多线程分发，默认10个进程

------------

#### · 版本及依赖组件

python3、pip3、PHP7.2、Mysql 7.0、node v12

#####依赖

> pip install scrapy

> pip install scrapy-redis

> pip install pymysql

> pip install PyExecJS

>pip install jieba

>pip install fake_useragent

#### ·更多爬虫

1、头条
2、百家号
3、微博
4、其他

#### ·SAAS计划
...